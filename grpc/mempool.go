package main

import (
	"encoding/hex"
	"errors"
	"fmt"
	"path/filepath"
	"time"

	pb "github.com/gcash/bchd/bchrpc/pb"
	"github.com/gcash/bchutil"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// ShuffleTxDetails ...
type ShuffleTxDetails struct {
	TxID        string
	BlockHeight int32
	BlockHash   string
	BlockTime   int64
	Date        string
	TotalIn     int64
	TotalOut    int64
	MinUTXO     int64
	OutputCount int
	PoolSize    int
	PoolName    string
}

func main() {
	mempool := make(chan *pb.Transaction)
	mempoolInfo := make(chan *pb.GetMempoolInfoResponse)
	blockchain := make(chan *pb.BlockInfo)

	go watchMempool(mempool)
	go watchMempoolInfo(mempoolInfo)
	go watchBlockchain(blockchain)

	for true {

		select {
		case info := <-mempoolInfo:
			fmt.Printf("MEMPOOL: %8d txs -> %8.2f kB\n", info.Size, (float64(info.Bytes) / 1024))
		case tx := <-mempool:
			//fmt.Println("TX:", hashString(reverse(tx.Hash)))
			if IsShuffleTX(tx) {
				details, err := ShuffleDetails(tx)
				if err != nil {
					fmt.Println(err)
				}
				fmt.Printf("UNCONFIRMED SHUFFLE TX: %s - %d IN -> %d OUT - MinUTXO: %d\n", details.TxID, details.PoolSize, details.OutputCount, details.MinUTXO)
			}
		case block := <-blockchain:
			fmt.Println("BLOCK:", hashString(reverse(block.Hash)))
		}
	}
}

func watchBlockchain(blockchain chan *pb.BlockInfo) {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	// stream, err := c.SubscribeTransactions(context.Background(), &pb.SubscribeTransactionsRequest{IncludeMempool: true, Subscribe: &pb.TransactionFilter{AllTransactions: true}})
	stream, err := c.SubscribeBlocks(context.Background(), &pb.SubscribeBlocksRequest{})
	if err != nil {
		fmt.Println(err)
	}

	notif := &pb.BlockNotification{}

Loop:
	for true {
		err = stream.RecvMsg(notif)
		if err != nil {
			fmt.Println(err)
			continue Loop
		}

		block := notif.Block
		blockchain <- block
	}
}

func watchMempoolInfo(mempool chan *pb.GetMempoolInfoResponse) {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

Loop:
	for true {
		infoResp, err := c.GetMempoolInfo(context.Background(), &pb.GetMempoolInfoRequest{})
		if err != nil {
			fmt.Println(err)
			continue Loop
		}

		mempool <- infoResp

		time.Sleep(10 * time.Second)
	}
}

func watchMempool(mempool chan *pb.Transaction) {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	stream, err := c.SubscribeTransactions(context.Background(), &pb.SubscribeTransactionsRequest{IncludeMempool: true, Subscribe: &pb.TransactionFilter{AllTransactions: true}})
	if err != nil {
		fmt.Println(err)
	}

	notif := &pb.TransactionNotification{}

Loop:
	for true {
		err = stream.RecvMsg(notif)
		if err != nil {
			fmt.Println(err)
			continue Loop
		}

		utx := notif.GetUnconfirmedTransaction()
		tx := utx.Transaction

		mempool <- tx
	}
}

// UTIL
var certificateFile = filepath.Join(bchutil.AppDataDir("bchd", false), "bcash.crt")

func initGRPC() (*grpc.ClientConn, error) {
	creds, err := credentials.NewClientTLSFromFile(certificateFile, "localhost")
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}
	conn, err := grpc.Dial("bchd.imaginary.cash:8335", grpc.WithTransportCredentials(creds), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(33554432)))
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	return conn, nil
}

func hashString(hash []byte) string {
	hashString := hex.EncodeToString(hash)

	return hashString
}

func reverse(slice []byte) []byte {
	for i := len(slice)/2 - 1; i >= 0; i-- {
		opp := len(slice) - 1 - i
		slice[i], slice[opp] = slice[opp], slice[i]
	}

	return slice
}

func getPoolName(minUTXO int64) (string, error) {
	if minUTXO > 10000 && minUTXO <= 100000 {
		return "0.0001", nil
	} else if minUTXO > 100000 && minUTXO <= 1000000 {
		return "0.001", nil
	} else if minUTXO > 1000000 && minUTXO <= 10000000 {
		return "0.01", nil
	} else if minUTXO > 10000000 && minUTXO <= 100000000 {
		return "0.1", nil
	} else if minUTXO > 100000000 && minUTXO <= 1000000000 {
		return "1", nil
	} else if minUTXO > 1000000000 && minUTXO <= 10000000000 {
		return "10", nil
	} else if minUTXO > 10000000000 && minUTXO <= 100000000000 {
		return "100", nil
	}

	return "", errors.New("ERROR: UTXO does not fit within pool contraints:" + string(minUTXO))
}

// GetMinInput ...
func GetMinInput(tx *pb.Transaction) int64 {
	minUTXO := int64(2100000000000000)

	for _, input := range tx.Inputs {
		if input.Value < minUTXO {
			minUTXO = input.Value
		}
	}

	return minUTXO
}

// IsShuffleTX ...
func IsShuffleTX(tx *pb.Transaction) bool {
	isShuffle := false
	identicalOutputs := make(map[int64]int)

	for _, output := range tx.Outputs {
		identicalOutputs[output.Value]++
	}

	inputCount := len(tx.Inputs)
	//fmt.Println("len(tx.Inputs)", len(tx.Inputs))
	outputCount := len(tx.Outputs)

	minInput := GetMinInput(tx)

	for key := range identicalOutputs {
		//fl, _ := strconv.ParseFloat(key, 64)
		minUTXO := key
		if minUTXO+270 == minInput && identicalOutputs[key] >= 3 && identicalOutputs[key] == inputCount && outputCount >= inputCount && outputCount <= (inputCount*2)-1 && minUTXO >= 10270 {
			//fmt.Println("identicalOutputs[key] == inputCount", identicalOutputs[key], inputCount)
			isShuffle = true
			break
		}
	}

	return isShuffle
}

// ShuffleDetails ...
func ShuffleDetails(tx *pb.Transaction) (*ShuffleTxDetails, error) {

	var details ShuffleTxDetails

	identicalOutputs := make(map[int64]int)
	for _, output := range tx.Outputs {
		identicalOutputs[output.Value]++
	}

	minInput := GetMinInput(tx)

	for outputValue := range identicalOutputs {
		inputCount := len(tx.Inputs)
		outputCount := len(tx.Outputs)
		minUTXO := outputValue
		if minUTXO+270 == minInput && identicalOutputs[outputValue] >= 3 && identicalOutputs[outputValue] == inputCount && outputCount >= inputCount && outputCount <= (inputCount*2)-1 && minUTXO >= 10270 {
			details.MinUTXO = minUTXO
		}
	}

	if details.MinUTXO == 0 {
		return nil, errors.New("gobitbox.ShuffleDetails: ERROR not a CashHuffle tx. txid: " + hex.EncodeToString(reverse(tx.Hash)))
	}

	poolName, err := getPoolName(details.MinUTXO)
	if err != nil {
		return nil, err
	}
	details.PoolName = poolName
	tm := time.Unix(tx.Timestamp, 0)
	date := tm.Format("2006-01-02")
	details.Date = date

	details.TxID = hex.EncodeToString(reverse(tx.Hash))
	details.BlockHash = hex.EncodeToString(reverse(tx.BlockHash))
	details.BlockHeight = tx.GetBlockHeight()
	details.BlockTime = tx.Timestamp

	var totalIn int64
	for _, in := range tx.GetInputs() {
		totalIn += in.Value
	}
	var totalOut int64
	for _, out := range tx.GetOutputs() {
		totalOut += out.Value
	}

	details.TotalIn = totalIn
	details.TotalOut = totalOut
	details.PoolSize = len(tx.Inputs)
	details.OutputCount = len(tx.Outputs)

	return &details, nil
}
