package main

import (
	"encoding/hex"
	"fmt"
	"path/filepath"
	"time"

	pb "github.com/gcash/bchd/bchrpc/pb"
	"github.com/gcash/bchutil"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {
	mempool := make(chan *pb.Transaction)
	mempoolInfo := make(chan *pb.GetMempoolInfoResponse)
	blockchain := make(chan *pb.BlockInfo)

	go watchMempool(mempool)
	go watchMempoolInfo(mempoolInfo)
	go watchBlockchain(blockchain)

	for true {

		select {
		case info := <-mempoolInfo:
			fmt.Printf("MEMPOOL: %8d txs -> %8.2f kB\n", info.Size, (float64(info.Bytes) / 1024))
		case tx := <-mempool:
			//fmt.Println("TX:", hashString(reverse(tx.Hash)))
			if IsShuffleTX(tx) {
				details, err := ShuffleDetails(tx)
				if err != nil {
					fmt.Println(err)
				}
				fmt.Printf("UNCONFIRMED SHUFFLE TX: %s - %d IN -> %d OUT - MinUTXO: %d\n", details.TxID, details.PoolSize, details.OutputCount, details.MinUTXO)
			}
		case block := <-blockchain:
			fmt.Println("BLOCK:", hashString(reverse(block.Hash)))
		}
	}
}

func watchBlockchain(blockchain chan *pb.BlockInfo) {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	// stream, err := c.SubscribeTransactions(context.Background(), &pb.SubscribeTransactionsRequest{IncludeMempool: true, Subscribe: &pb.TransactionFilter{AllTransactions: true}})
	stream, err := c.SubscribeBlocks(context.Background(), &pb.SubscribeBlocksRequest{})
	if err != nil {
		fmt.Println(err)
	}

	notif := &pb.BlockNotification{}

Loop:
	for true {
		err = stream.RecvMsg(notif)
		if err != nil {
			fmt.Println(err)
			continue Loop
		}

		block := notif.Block
		blockchain <- block
	}
}

func watchMempoolInfo(mempool chan *pb.GetMempoolInfoResponse) {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

Loop:
	for true {
		infoResp, err := c.GetMempoolInfo(context.Background(), &pb.GetMempoolInfoRequest{})
		if err != nil {
			fmt.Println(err)
			continue Loop
		}

		mempool <- infoResp

		time.Sleep(10 * time.Second)
	}
}

func watchMempool(mempool chan *pb.Transaction) {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	stream, err := c.SubscribeTransactions(context.Background(), &pb.SubscribeTransactionsRequest{IncludeMempool: true, Subscribe: &pb.TransactionFilter{AllTransactions: true}})
	if err != nil {
		fmt.Println(err)
	}

	notif := &pb.TransactionNotification{}

Loop:
	for true {
		err = stream.RecvMsg(notif)
		if err != nil {
			fmt.Println(err)
			continue Loop
		}

		utx := notif.GetUnconfirmedTransaction()
		tx := utx.Transaction

		mempool <- tx
	}
}

// UTIL
var certificateFile = filepath.Join(bchutil.AppDataDir("bchd", false), "bcash.crt")

func initGRPC() (*grpc.ClientConn, error) {
	creds, err := credentials.NewClientTLSFromFile(certificateFile, "localhost")
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}
	conn, err := grpc.Dial("bchd.imaginary.cash:8335", grpc.WithTransportCredentials(creds), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(33554432)))
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	return conn, nil
}

func hashString(hash []byte) string {
	hashString := hex.EncodeToString(hash)

	return hashString
}

func reverse(slice []byte) []byte {
	for i := len(slice)/2 - 1; i >= 0; i-- {
		opp := len(slice) - 1 - i
		slice[i], slice[opp] = slice[opp], slice[i]
	}

	return slice
}
