import React from 'react';
import './App.css';
import Live from './Live';

function App() {
  return (
    <div className="App">
      <Live />
    </div>
  );
}

export default App;
