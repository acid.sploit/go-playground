package main

import (
	"fmt"
	"path/filepath"

	pb "github.com/gcash/bchd/bchrpc/pb"
	"github.com/gcash/bchutil"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const grpcServer = "bchd.greyh.at:8335"

var certificateFile = filepath.Join(bchutil.AppDataDir("bchd", false), "bchdgreyhat.crt")

func initGRPC() (*grpc.ClientConn, error) {
	// creds, err := credentials.NewClientTLSFromFile(certificateFile, "bchd.greyh.at")
	// if err != nil {
	// 	//fmt.Println(err)
	// 	return nil, err
	// }
	conn, err := grpc.Dial(grpcServer, grpc.WithTransportCredentials(credentials.NewClientTLSFromCert(nil, "")))
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	return conn, nil
}

func main() {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	blockchainInfoResp, err := c.GetBlockchainInfo(context.Background(), &pb.GetBlockchainInfoRequest{})
	if err != nil {
		fmt.Println(err)
	}

	bestHeight := blockchainInfoResp.GetBestHeight()
	bestBlockHash := blockchainInfoResp.GetBestBlockHash()

	fmt.Printf("%d - %x\n", bestHeight, bestBlockHash)

	fmt.Printf("%v\n", bestBlockHash)
	fmt.Printf("%#v\n", bestBlockHash)
}
