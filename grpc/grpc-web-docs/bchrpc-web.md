# Access BCHD's gRPC API (bchrpc) from the Browser

Yes, we can now access all the goodies like mempool info, txindex, addressindex and much [more](https://medium.com/@bchd.cash/announcing-the-bchd-grpc-api-16b3a833292e) directly from a full node and straight from the browser. This with some JavaScript-fu, but even then, the most labour intensive code (type definition, client, binary-to-object, ...) is code-generated for your convenience.

## Introduction
In this tutorial we'll go through setting up your dev environmnet, build a simple live mempool monitor and watch a bitcoin cash address for incoming payments.

## Setup dev environment
In order to get talking to the bchrpc we first need to generate the javascript libs with `protoc` or download the latest version from the repo.

### Go & Protoc
* Setup your go dev env as described [here](https://wiki.archlinux.org/index.php/Go)
* Install `protobuf` through your system's package manager
* Install `protoc-gen-go` with `go get` and `go install`

```
> pacman, apt, yum, <protobuf package>
> go get -d -u github.com/golang/protobuf/protoc-gen-go
> go install github.com/golang/protobuf/protoc-gen-go
```

### Init ReactJS app
* Init [ReactJS](https://reactjs.org/docs/create-a-new-react-app.html) project
* Install [grpc-web](https://github.com/improbable-eng/grpc-web) dependencies


```
> npx create-react-app mempool-monitor
> cd mempool-monitor/src/
> yarn add ts-protoc-gen
> yarn add @improbable-eng/grpc-web
> yarn add @types/google-protobuf
> yarn add google-protobuf
> yarn add react-materialize
```

### Generate JS libs

* Download the `.proto` file from GitHub: [bchrpc.proto](https://github.com/gcash/bchd/blob/master/bchrpc/bchrpc.proto)
* Generate the libs with `protoc`


```
> cd src/
> wget https://raw.githubusercontent.com/gcash/bchd/master/bchrpc/bchrpc.proto

> mkdir bchrpc
> protoc \
  --plugin=protoc-gen-ts=../node_modules/.bin/protoc-gen-ts \
  --js_out=import_style=commonjs,binary:bchrpc \
  --ts_out=service=true:bchrpc \
  ./bchrpc.proto
```

Finally add `/* eslint-disable */` at the top of the generated .js libs.
* bchrpc/bchrpc_pb_service.js
* bchrpc/bchrpc_pb.js

