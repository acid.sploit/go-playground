package main

import (
	"fmt"
	//"github.com/bchsuite/bchd/btcec"
	// "github.com/bchsuite/bchd/chaincfg"
	// "github.com/bchsuite/bchutil"
	// "github.com/bchsuite/bchutil/hdkeychain"
	b39 "github.com/tyler-smith/go-bip39"
)

func main() {
	ent, err := b39.NewEntropy(128)
		if err != nil {
			return
		}
	mnemonic, err := b39.NewMnemonic(ent)
	if err != nil {
		return
	}

	fmt.Println(mnemonic)

	seed := b39.NewSeed(mnemonic, "")
	fmt.Println(seed)


}