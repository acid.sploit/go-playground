package main

import (
	"fmt"

	pb "github.com/gcash/bchd/bchrpc/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const grpcServer = "bchd.greyh.at:8335"

func main() {
	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	blockchainInfoResp, err := c.GetBlockchainInfo(context.Background(), &pb.GetBlockchainInfoRequest{})
	if err != nil {
		fmt.Println(err)
	}

	bestHeight := blockchainInfoResp.GetBestHeight()
	bestBlockHash := blockchainInfoResp.GetBestBlockHash()

	fmt.Printf("%d - %x\n", bestHeight, bestBlockHash)

	fmt.Println("GRPC RESPONSE:")
	fmt.Printf("%v\n", bestBlockHash)
	fmt.Printf("%#v\n", bestBlockHash)
	fmt.Printf("%x\n", bestBlockHash)

	bestBlockHash = reverse(bestBlockHash)
	fmt.Println("REVERSED:")
	fmt.Printf("%v\n", bestBlockHash)
	fmt.Printf("%#v\n", bestBlockHash)
	fmt.Printf("%x\n", bestBlockHash)

}

func initGRPC() (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(grpcServer, grpc.WithTransportCredentials(credentials.NewClientTLSFromCert(nil, "")))
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func reverse(slice []byte) []byte {
	for i := len(slice)/2 - 1; i >= 0; i-- {
		opp := len(slice) - 1 - i
		slice[i], slice[opp] = slice[opp], slice[i]
	}

	return slice
}
