package main

import (
	"encoding/hex"
	"fmt"
	"path/filepath"

	pb "github.com/gcash/bchd/bchrpc/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"github.com/gcash/bchutil"
)

var certificateFile = filepath.Join(bchutil.AppDataDir("bchd", false), "bcash.crt")

func initGRPC() (*grpc.ClientConn, error) {
	creds, err := credentials.NewClientTLSFromFile(certificateFile, "localhost")
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}
	conn, err := grpc.Dial("bchd.imaginary.cash:8335", grpc.WithTransportCredentials(creds), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(33554432)))
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	return conn, nil
}

func main() {

	conn, err := initGRPC()
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	c := pb.NewBchrpcClient(conn)

	hashStr := "00000000000000000224e915f7106516955a3aa13eb9b3157e868d820a44591c"
	hash, err := hex.DecodeString(hashStr)
	if err != nil {
		fmt.Println(err)
	}
	hash = reverse(hash)

	blockResp, err := c.GetBlock(context.Background(), &pb.GetBlockRequest{FullTransactions: true, HashOrHeight: &pb.GetBlockRequest_Hash{Hash: hash}})
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("BLOCK OBJECT: %s - %8.2f kB\n", hashStr, (float64(blockResp.XXX_Size()) / 1024))

	blockRwaResp, err := c.GetRawBlock(context.Background(), &pb.GetRawBlockRequest{HashOrHeight: &pb.GetRawBlockRequest_Hash{Hash: hash}})
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("BLOCK RAW   : %s - %8.2f kB\n", hashStr, (float64(blockRwaResp.XXX_Size()) / 1024))

}

func hashString(hash []byte) string {
	hashString := hex.EncodeToString(hash)

	return hashString
}

func reverse(slice []byte) []byte {
	for i := len(slice)/2 - 1; i >= 0; i-- {
		opp := len(slice) - 1 - i
		slice[i], slice[opp] = slice[opp], slice[i]
	}

	return slice
}
